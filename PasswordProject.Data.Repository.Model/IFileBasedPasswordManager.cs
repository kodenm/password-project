﻿namespace PasswordProject.Data.Repository.Model;

public interface IFileBasedPasswordManager : IPasswordManager
{
    void Save();
}
