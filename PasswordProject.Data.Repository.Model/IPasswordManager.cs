﻿using PasswordProject.Data.Model.Entities;

namespace PasswordProject.Data.Repository.Model;

public interface IPasswordManager
{
    IEnumerable<Account> GetAll();
    Account? GetById(Guid id);
    void Add(Account account);
    void AddRange(IEnumerable<Account> accounts);
    void Remove(Guid id);
    void RemoveAll();
    void Update(Account account);
}
