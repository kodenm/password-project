﻿namespace PasswordProject.Data.Model.Enums;

public enum IdentifierType
{
    Login,
    Email,
    Phone,
    Custom
}
