﻿using PasswordProject.Data.Model.Enums;

namespace PasswordProject.Data.Model.Entities;

public class Identifier
{
    public Guid Id { get; set; }
    public IdentifierType Type { get; set; }
    public string Value { get; set; } = string.Empty;
}
