﻿namespace PasswordProject.Data.Model.Entities;

public class Account
{
    public Guid Guid { get; set; }
    public string Name { get; set; } = string.Empty;
    public Guid? DefaultIdentifierId { get; set; } = null;
    public string Password { get; set; } = string.Empty;
    public string Source { get; set; } = string.Empty;
    public List<Identifier> Identifiers { get; set; } = new List<Identifier>();
}
