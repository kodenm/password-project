﻿using PasswordProject.Data.Model.Entities;
using PasswordProject.Data.Repository.Model;
using System.Text.Json;

namespace PasswordProject.Data.Repository.Json;

public class JsonFilePasswordManager : IFileBasedPasswordManager
{
    private readonly string _path;
    private readonly List<Account> _accounts;

    public JsonFilePasswordManager(string path)
    {
        _path = path;

        try
        {
            using var streamReader = new StreamReader(_path);
            var json = streamReader.ReadToEnd();
            _accounts = JsonSerializer.Deserialize<List<Account>>(json) ?? new List<Account>();
        }
        catch
        {
            _accounts = new List<Account>();
            Save();
        }
    }

    public void Add(Account account)
    {
        if (_accounts.Any(a => a.Guid == account.Guid))
        {
            Update(account);
        }
        else
        {
            _accounts.Add(account);
        }
    }

    public void AddRange(IEnumerable<Account> accounts)
    {
        var unionAccounts = accounts.UnionBy(_accounts, account => account.Guid);
        if (unionAccounts.Any())
        {
            accounts = accounts.Except(unionAccounts);
        }

        _accounts.AddRange(accounts);
    }

    public IEnumerable<Account> GetAll()
    {
        return _accounts;
    }

    public Account? GetById(Guid id)
    {
        return _accounts.Find(a => a.Guid == id);
    }

    public void Remove(Guid id)
    {
        var account = GetById(id);
        if (account != null)
        {
            _accounts.Remove(account);
        }
    }

    public void RemoveAll()
    {
        _accounts.Clear();
    }

    public void Update(Account account)
    {
        // maybe it's better to reassign fields of the existing object from the given one
        var oldAccount = GetById(account.Guid);
        if (oldAccount != null)
        {
            _accounts.Remove(oldAccount);
            _accounts.Add(account);
        }
    }

    public async void Save()
    {
        using var streamReader = File.CreateText(_path);
        await streamReader.WriteAsync(JsonSerializer.Serialize(_accounts, new JsonSerializerOptions { WriteIndented = true }));
    }
}
