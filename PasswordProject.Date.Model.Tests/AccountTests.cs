using AutoFixture;
using NUnit.Framework;
using PasswordProject.Data.Model.Entities;
using PasswordProject.Data.Model.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PasswordProject.Date.Model.Tests;

[TestFixture]
public class AccountTests
{
    private static readonly Fixture _fixture = new Fixture();

    [Test]
    public void AccountShouldBeAbleToStoreMultipleIdentifiers()
    {
        // arrange
        var initialIdentifierList = new List<Identifier>()
            {
                new Identifier { Id = Guid.NewGuid(), Type = IdentifierType.Login, Value = _fixture.Create<string>() },
                new Identifier { Id = Guid.NewGuid(), Type = IdentifierType.Email, Value = _fixture.Create<string>() },
                new Identifier { Id = Guid.NewGuid(), Type = IdentifierType.Phone, Value = _fixture.Create<string>() }
            };
        var account = new Account
        {
            Guid = Guid.NewGuid(),
            Source = _fixture.Create<string>(),
            Identifiers = initialIdentifierList,
            DefaultIdentifierId = initialIdentifierList.FirstOrDefault()?.Id ?? Guid.Empty,
            Password = _fixture.Create<string>()
        };

        // act
        var expectedIdentifierList = initialIdentifierList.Select(identifier => identifier);

        // assert
        Assert.AreEqual(expectedIdentifierList, account.Identifiers);
    }

    [Test]
    public void AccountCanHaveDefaultIdentifier()
    {
        // arrange
        var initialIdentifierList = new List<Identifier>()
            {
                new Identifier { Id = Guid.NewGuid(), Type = IdentifierType.Login, Value = _fixture.Create<string>() },
                new Identifier { Id = Guid.NewGuid(), Type = IdentifierType.Email, Value = _fixture.Create<string>() },
                new Identifier { Id = Guid.NewGuid(), Type = IdentifierType.Phone, Value = _fixture.Create<string>() }
            };
        var account = new Account
        {
            Guid = Guid.NewGuid(),
            Source = _fixture.Create<string>(),
            Identifiers = initialIdentifierList,
            DefaultIdentifierId = initialIdentifierList.First().Id,
            Password = _fixture.Create<string>()
        };

        // act
        var expectedDefaultIdentifierId = initialIdentifierList.First().Id;

        // assert
        Assert.AreEqual(expectedDefaultIdentifierId, account.DefaultIdentifierId);
    }

    [Test]
    public void AccountCanSwitchDefaultIdentifier()
    {
        // arrange
        var initialIdentifierList = new List<Identifier>()
            {
                new Identifier { Id = Guid.NewGuid(), Type = IdentifierType.Login, Value = _fixture.Create<string>() },
                new Identifier { Id = Guid.NewGuid(), Type = IdentifierType.Email, Value = _fixture.Create<string>() },
                new Identifier { Id = Guid.NewGuid(), Type = IdentifierType.Phone, Value = _fixture.Create<string>() }
            };
        var account = new Account
        {
            Guid = Guid.NewGuid(),
            Source = _fixture.Create<string>(),
            Identifiers = initialIdentifierList,
            DefaultIdentifierId = Guid.Empty,
            Password = _fixture.Create<string>()
        };

        // act
        TestDelegate firstDefaultIdentifierSetting = () => account.DefaultIdentifierId = account.Identifiers.First(i => i.Type == IdentifierType.Login).Id;
        var firstExpectedDefaultIdentifierId = initialIdentifierList.First(i => i.Type == IdentifierType.Login).Id;

        TestDelegate secondDefaultIdentifierSetting = () => account.DefaultIdentifierId = account.Identifiers.First(i => i.Type == IdentifierType.Email).Id;
        var secondExpectedDefaultIdentifierId = initialIdentifierList.First(i => i.Type == IdentifierType.Email).Id;

        // assert
        Assert.DoesNotThrow(firstDefaultIdentifierSetting);
        Assert.AreEqual(firstExpectedDefaultIdentifierId, account.DefaultIdentifierId);

        Assert.DoesNotThrow(secondDefaultIdentifierSetting);
        Assert.AreEqual(secondExpectedDefaultIdentifierId, account.DefaultIdentifierId);
    }

    [Test]
    public void AccountCanHaveEmptyDefaultIdentifierId()
    {
        // arrange
        var initialIdentifierList = new List<Identifier>()
            {
                new Identifier { Id = Guid.NewGuid(), Type = IdentifierType.Login, Value = _fixture.Create<string>() },
                new Identifier { Id = Guid.NewGuid(), Type = IdentifierType.Email, Value = _fixture.Create<string>() },
                new Identifier { Id = Guid.NewGuid(), Type = IdentifierType.Phone, Value = _fixture.Create<string>() }
            };
        var account = new Account
        {
            Guid = Guid.NewGuid(),
            Source = _fixture.Create<string>(),
            Identifiers = initialIdentifierList,
            DefaultIdentifierId = null,
            Password = _fixture.Create<string>()
        };

        // act
        // assert
        Assert.IsFalse(account.DefaultIdentifierId.HasValue);
    }

    [Test]
    public void AccountCanHaveMultipleIdentifiersOfTheSameType()
    {
        // arrange
        var initialIdentifierList = new List<Identifier>()
            {
                new Identifier { Id = Guid.NewGuid(), Type = IdentifierType.Login, Value = _fixture.Create<string>() },
                new Identifier { Id = Guid.NewGuid(), Type = IdentifierType.Login, Value = _fixture.Create<string>() }
            };
        var account = new Account
        {
            Guid = Guid.NewGuid(),
            Source = _fixture.Create<string>(),
            Identifiers = initialIdentifierList,
            DefaultIdentifierId = null,
            Password = _fixture.Create<string>()
        };

        // act
        var expectedIdentifierList = initialIdentifierList.Select(identifier => identifier);

        // assert
        Assert.AreEqual(expectedIdentifierList, account.Identifiers);
    }
}
